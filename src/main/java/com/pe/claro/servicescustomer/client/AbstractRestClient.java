package com.pe.claro.servicescustomer.client;

import com.pe.claro.servicescustomer.client.RestClient;
import  com.pe.claro.servicescustomer.client.RestClient.RestClientBuilder;

public class AbstractRestClient {
	
	protected RestClient restClient;

    public void loadRestClient(String url, int connectionTimeout) {
    	restClient= new RestClientBuilder(url).connectionTimeoutBuilder(connectionTimeout).build();
    }

	public RestClient getRestClient() {
		return restClient;
	}
	
}
