package com.pe.claro.servicescustomer.client;

import com.pe.claro.servicescustomer.entity.Token;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
		String url ="https://service.imiclaroapp.com.pe/clarohogar/api/generatoken";
		AbstractRestClient abstractRestClient = new AbstractRestClient();
		
		abstractRestClient.loadRestClient(url, 10000);
		
		
		RestClient restClient = abstractRestClient.getRestClient();
		
		Token token = new Token();
		token.setClientId("a06165c7-b9ba-43da-bb59-2d0083cd8e14");
		token.setSecretId("b3f3dc06-f762-45ab-9bd8-3da060612839");
		String requests = "{\"client_id\":\"a06165c7-b9ba-43da-bb59-2d0083cd8e14\",\"client_secret\":\"b3f3dc06-f762-45ab-9bd8-3da060612839\"}";
		//ClientResponse response=restClient.post(url, requests, null);
		
		//System.out.println("RESTCLIENT ?????? "+response.getStatus());
		
		Client client = Client.create();

		WebResource webResource = client
		   .resource("https://service.imiclaroapp.com.pe/clarohogar/api/generatoken");

		String input = "{\"singer\":\"Metallica\",\"title\":\"Fade To Black\"}";

		ClientResponse response = webResource.type("application/json")
		   .post(ClientResponse.class, requests);

		if (response.getStatus() != 201) {
			throw new RuntimeException("Failed : HTTP error code : "
			     + response.getStatus());
		}

		System.out.println("Output from Server .... \n");
		String output = response.getEntity(String.class);
		System.out.println(output);
		
		}catch(Exception e ){
			e.printStackTrace();
		}
	}

}
