package com.pe.claro.servicescustomer.client;

public enum AuthenticationMode {
	BASIC_AUTH, SHARED_SECRET_KEY
}
