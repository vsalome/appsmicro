package com.pe.claro.servicescustomer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServicescustomerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServicescustomerApplication.class, args);
	}

}
